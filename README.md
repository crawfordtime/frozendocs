![Frozen Fist](https://i.pinimg.com/736x/28/5e/5c/285e5cb8027183aa5f53864d2b8c379b--concept-art-vehicles.jpg)

# Frozen Fist

## Vortex ops

### Pushed to BitBucket repository -  01/11/2018

Currently I am working on a project called Frozen Fist. During this project I will be developing software to operate semi-autonomous armored assault vehicles. The vehicles are designed to operate either remotely with a human pilot or in "robot mode" with guidance from an on-board Artificial Intelligence (AI) system.

A project by [Chris Crawford](chris.crawford@smail.rasmussen.edu) 

### Modified README on BitBucket - 01/18/2018
